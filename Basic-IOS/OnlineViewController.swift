//
//  OnlineViewController.swift
//  Basic-IOS
//
//  Created by Savdeep on 04/03/2019.
//  Copyright © 2019 Theoplayer.com. All rights reserved.
//

import UIKit
import THEOplayerSDK
import GCDWebServers

class OnlineViewController: UIViewController {
    var theoplayer: THEOplayer!
    var listeners : [String: EventListener] = [:]
    
    
    
    //Status Bar
    @IBOutlet weak var statusbar: UILabel!

    
    @IBOutlet weak var viewerPlay: UIView!
   
    //Initialising the Player
    func setupTheoPlayer() {
        var frame: CGRect = UIScreen.main.bounds
        frame.size.height = 350
        let stylePath1 = Bundle.main.path(forResource:"style1", ofType: "css")!
        let stylePath2 = Bundle.main.path(forResource:"font-awesome.min", ofType: "css")!
        let scriptPath = Bundle.main.path(forResource:"script1", ofType: "js")!
        let playerConfig = THEOplayerConfiguration(defaultCSS: true, cssPaths:[stylePath1, stylePath2], jsPaths: [scriptPath],pictureInPicture: true)
        self.theoplayer = THEOplayer(configuration: playerConfig)
        self.theoplayer.frame =  frame
        self.theoplayer.addAsSubview(of: viewerPlay)
        theoplayer.pip!.configure(movable: false, defaultCorner: .bottomRight, scale: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupTheoPlayer()
        
        self.theoplayer.source = sampleSource
        theoplayer.play();
        self.statusbar.text = "Status: Playing Online"
     
    }
    @IBAction func backtoMain(_ sender: Any) {
        self.theoplayer.destroy()
    }
    
    @IBAction func PauseVideo(_ sender: Any) {
        
        cachingTask?.pause()
        self.statusbar.text = "Status: Paused"
    }
    
    @IBAction func ResumeDownload(_ sender: Any) {
        cachingTask?.start()
    }
    
    @IBAction func DownloadVideo(_ sender: Any) {
        // Download the subtitles (if not already downloaded) and add them to the textTracks array
        downloadSubtitle(language: "English", url: engSub!)
        downloadSubtitle(language: "Arabic", url: arSub!)
        
        //Start the Caching Start
        cacheSource(source: videoSrc, expirationDate: Date.distantFuture)
        
    }
    
    
    var sampleSource: SourceDescription {
        let src = "https://content.uplynk.com/ext/12ccd788e5224b0692264ac02cc4929f/stg-6845260.m3u8?rmt=fps"
        let certificate = "https://x-drm.uplynk.com/fairplay/public_key/12ccd788e5224b0692264ac02cc4929f_new.cer"
        let stream = "application/x-mpegurl"
        let drmConfig = UplynkDRMConfiguration(certificateURL: certificate)
        let textTrack1 = TextTrackDescription(src: "https://s3-eu-west-1.amazonaws.com/theoplayer-cdn/demos/theoplayer-web/1060465_6846477.webvtt", srclang: "English", isDefault: true, kind: .subtitles, label:"English")
        let textTrack2 = TextTrackDescription(src: "https://s3-eu-west-1.amazonaws.com/theoplayer-cdn/demos/theoplayer-web/1060465_6846478_96.webvtt", srclang: "Arabic", isDefault: false, kind: .subtitles, label:"Arabic")
        
        return SourceDescription(
            source: TypedSource(
                src: src,
                type: stream,
                drm: drmConfig
            ),
            textTracks: [textTrack1,textTrack2]
            
        )
        
    }
    
    
    //Download a Video and Subtitles
    
    //Subtitles URL
    let engSub = URL(string: "https://s3-eu-west-1.amazonaws.com/theoplayer-cdn/demos/theoplayer-web/1060465_6846477.webvtt")
    let arSub = URL(string: "https://s3-eu-west-1.amazonaws.com/theoplayer-cdn/demos/theoplayer-web/1060465_6846478_96.webvtt")
    let localServer = "https://127.0.0.1:80/"
    
    //Source to Video
    let videoSrc = "https://content.uplynk.com/ext/12ccd788e5224b0692264ac02cc4929f/stg-6845260.m3u8?rmt=fps&fp.persistence=1&fp.rental_duration=86400&fp.lease_duration=86400&rays=a"
    let drmConfig = UplynkDRMConfiguration(certificateURL: "https://x-drm.uplynk.com/fairplay/public_key/12ccd788e5224b0692264ac02cc4929f_new.cer")
    
    //TextTracks
    var subtitles : [TextTrackDescription] = []
    
    //creating variable for caching Task
    var cachingTask: CachingTask?
    
    //Directory to Download the Subtitles
    public func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func downloadSubtitle(language: String, url: URL) {
        let name = "\(language)-subtitle.webvtt"
        let fileManager = FileManager.default
        let documentsUrl = getDocumentsDirectory()
        let downloadedSubtitlePath = documentsUrl.appendingPathComponent(name).path
        let localUrl = "\(localServer)\(name)"
        
        if fileManager.fileExists(atPath: downloadedSubtitlePath) {
            print("\(language) subs already downloaded!")
            let textTrack = TextTrackDescription(src: localUrl, srclang: language, isDefault: false, kind: .subtitles, label: language)
            self.subtitles.append(textTrack)
        } else {
            self.DownloadFromUrl(subtitleUrl: url, subtitleName: name, language: language)
        }
    }
    
    // Subtitle download logic
    func DownloadFromUrl(subtitleUrl : URL, subtitleName : String, language : String) {
        // Create destination URL
        let name = "\(language)-subtitle.webvtt"
        let documentsUrl = getDocumentsDirectory()
        let downloadSubtitlePath = documentsUrl.appendingPathComponent(name)
        let localUrl = "\(localServer)\(name)"
        
        
        //Create URL to the source file you want to download
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url: subtitleUrl)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded \(subtitleName). Status code: \(statusCode) at location: \(subtitleUrl)")
                    
                    let textTrack = TextTrackDescription(src: localUrl, srclang: language, isDefault: false, kind: .subtitles, label: language)
                    self.subtitles.append(textTrack)
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: downloadSubtitlePath)
                } catch (let writeError) {
                    print("Error creating a file \(downloadSubtitlePath) : \(writeError)")
                }
                
            } else {
                print("Error took place while downloading the file \(subtitleName). Error description: %@", error?.localizedDescription as Any);
            }
        }
        task.resume()
    }
    
    func checkIfTaskExists(source: String, expirationDate: Date) -> CachingTask {
        for cachedTask in THEOplayer.cache.tasks {
            if cachedTask.source.sources[0].src == URL(string: source) && cachedTask.status == CachingTaskStatus.error {
                print("Removing task with source \(source)")
                cachedTask.remove()
            }
            else if cachedTask.source.sources[0].src == URL(string: source) && cachedTask.status == CachingTaskStatus.done  {
                self.statusbar.text = "Status: Downloaded"
                self.theoplayer.source = self.createSourceDescription(drmConfig: self.drmConfig, src: source, textTracks: self.subtitles)
                return cachedTask
            }
            else  {
                self.statusbar.text = "Status: Downloading"
                self.theoplayer.source = self.createSourceDescription(drmConfig: self.drmConfig, src: source, textTracks: self.subtitles)
                return cachedTask
            }
        }
        cachingTask = THEOplayer.cache.createTask(source: self.createSourceDescription(drmConfig: self.drmConfig, src: source), parameters: CachingParameters(expirationDate: expirationDate))
        print("Did create caching task \(cachingTask!.source.sources[0].src)")
        self.statusbar.text = "Status: Downloading"
        return cachingTask!
    }
    
    func cacheSource(source: String, expirationDate: Date) {
        
        cachingTask = checkIfTaskExists(source: source, expirationDate: expirationDate)
        
        if cachingTask != nil {
            _ = cachingTask!.addEventListener(type: CachingTaskEventTypes.STATE_CHANGE) { event in
                print("Received state change on caching task \(self.cachingTask!.source.sources[0].src) Status: \(self.cachingTask!.status)")
                
                if self.cachingTask!.status == CachingTaskStatus.done{
                    self.theoplayer.source = self.createSourceDescription(drmConfig: self.drmConfig, src: source, textTracks: self.subtitles)
                    self.statusbar.text = "Status: Downloaded"
                }
            }
            _ = cachingTask!.addEventListener(type: CachingTaskEventTypes.PROGRESS) { event in
                print("Received progress on caching task \(self.cachingTask!.source.sources[0].src) Cached: ")
                for timeRange in self.cachingTask!.cached {
                    print(timeRange.start, timeRange.end)
                }
                
                print("CachingTaskEventTypes.PROGRESS: %@", String(format:"%.2f", self.cachingTask!.percentageCached))
                self.statusbar.text = "Status: \(String(format:"%.2f", self.cachingTask!.percentageCached*100.0))%"
            }
            cachingTask!.start()
        }
    }
    
    // Dynamically create source description
    func createSourceDescription(drmConfig: UplynkDRMConfiguration, src : String, textTracks : [TextTrackDescription]? = nil)  -> SourceDescription {
        let typedSource = TypedSource(
            src: src,
            type: "application/x-mpegurl",
            drm: drmConfig
        )
        
        if textTracks == nil {
            return SourceDescription(
                source: typedSource
            )
        }
        return SourceDescription(
            source: typedSource,
            textTracks: textTracks
        )
    }
}
