//
//  TheoRequestInterceptor.swift
//
//
//  Created by Savdeep Singh Gandhi on 07/03/2019.
//  Copyright © 2019 THEOplayer. All rights reserved.
//

import Foundation
import SystemConfiguration

class TheoRequestInterceptor : URLProtocol {
    
    static let keyTheoRequestInterceptorProperty = "keyTheoRequestInterceptorProperty"
    static let supportedProtocols = ["https"]
    
    
    public static func registerInterceptor() {
        
        for supportedProtocol in supportedProtocols {
            URLProtocol.wk_registerScheme(supportedProtocol);
        }
        
        URLProtocol.registerClass(TheoRequestInterceptor.self)
    }
    
    override class func canInit(with request: URLRequest) -> Bool {
        print("Request URL = \(String(describing: request.url?.absoluteString))")

        return (request.url?.absoluteString.contains("127.0.0.1"))!
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override class func requestIsCacheEquivalent(_ a: URLRequest, to b: URLRequest) -> Bool {
        return super.requestIsCacheEquivalent(a, to: b)
    }

    override func startLoading() {
        //set property to identify our request and don't fall into infinite loop
        
        //TODO: eliminate force casting issues here
        let mutableRequest = ((self.request as NSURLRequest).mutableCopy() as? NSMutableURLRequest)!
        URLProtocol.setProperty(true, forKey: TheoRequestInterceptor.keyTheoRequestInterceptorProperty, in: mutableRequest)
        
        do {
            
            let fileData = try Data(contentsOf: getDocumentsDirectory().appendingPathComponent(self.request.url!.lastPathComponent))
           
            var headers : [String : String] = [:]
            headers["Content-Type"] = "text/vtt"
            headers["Content-Length"] = "\(fileData.count)"
            headers["Access-Control-Allow-Origin"] = "*"
            let response = HTTPURLResponse(url: self.request.url!, statusCode: 200, httpVersion: "1.1", headerFields: headers)
            self.client?.urlProtocol(self, didReceive: response!, cacheStoragePolicy: .notAllowed)
            self.client?.urlProtocol(self, didLoad: fileData)
            self.client?.urlProtocolDidFinishLoading(self)
        
        } catch let catchedError {
            print("#ERROR:", catchedError)
        }
    }
    
    public func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    override func stopLoading() {
        
    }

}
