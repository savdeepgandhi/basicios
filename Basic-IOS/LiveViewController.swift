//
//  LiveViewController.swift
//  Basic-IOS
//
//  Created by Savdeep on 13/05/2019.
//  Copyright © 2019 Theoplayer.com. All rights reserved.
//

import UIKit
import THEOplayerSDK

class LiveViewController: UIViewController {
    
    var theoplayer: THEOplayer!
    var listeners : [String: EventListener] = [:]
    
    @IBOutlet weak var PlayerView: UIView!
    func setupTheoPlayer() {
        var frame: CGRect = UIScreen.main.bounds
        frame.size.height = 350
        
        self.theoplayer = THEOplayer()
        self.theoplayer.frame =  frame
        self.theoplayer.addAsSubview(of: PlayerView)
    }
    
    @IBAction func backtoMain(_ sender: Any) {
        self.theoplayer.destroy()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Setup THEO Player
        setupTheoPlayer()
        
        //Request for Network Interceptor for Downloaded Subtitles
        TheoRequestInterceptor.registerInterceptor()
        self.theoplayer.source = sampleSource
        theoplayer.play();
    }
    
    var sampleSource: SourceDescription {

        let src = "https://content.uplynk.com/channel/fd7ab9ab25e24b22851a9f7e10cb637b.m3u8"
        let stream = "application/x-mpegurl"
        return SourceDescription(
            source: TypedSource(
                src: src,
                type: stream
            )
            
        )
        
    }
   

}
