//
//  
//
//
//  Created by Savdeep Singh Gandhi on 07/03/2019.
//  Copyright © 2019 THEOplayer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLProtocol (WKWebViewSupport)

+ (void)wk_registerScheme:(NSString *)scheme;
+ (void)wk_unregisterScheme:(NSString *)scheme;

@end
