//
//  TasksViewController.swift
//  Basic-IOS
//
//  Created by Savdeep on 04/03/2019.
//  Copyright © 2019 Theoplayer.com. All rights reserved.
//

import UIKit
import THEOplayerSDK

class TasksViewController: UIViewController {
    var theoplayer: THEOplayer!
    var listeners : [String: EventListener] = [:]
    var buttonY: CGFloat = 200
    
    @IBOutlet weak var taskListView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getTaskList()
    }
    
    @IBAction func backtoMain(_ sender: Any) {
        //   self.theoplayer.destroy()
    }
    
    
    func getTaskList(){
        for cachedTask in THEOplayer.cache.tasks {
            let srcButton = UIButton(frame: CGRect(x: 10, y: buttonY, width: 350, height: 50))
            buttonY = buttonY + 70  // we are going to space these UIButtons 70px apart
            
            srcButton.layer.cornerRadius = 10  // get some fancy pantsy rounding
            srcButton.backgroundColor = UIColor.lightGray
            srcButton.setTitle("\(cachedTask.source.sources[0].src)", for: UIControl.State.normal)
            srcButton.titleLabel?.text = "\(cachedTask.source.sources[0].src)"
            srcButton.addTarget(self, action: #selector(srcButtonPressed), for: .touchUpInside)
            
            self.view.addSubview(srcButton)  // myView in this case is the view you want these buttons added
        }
    }
    
    @objc func srcButtonPressed(_ sender:UIButton!) {
        let offlinecontroller = OfflineViewController()
        if sender.titleLabel?.text != nil {
            let sourceUrl = URL(string: (sender.titleLabel?.text)!)
            offlinecontroller.removeSourceFromCache(source: sourceUrl!)
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            
        } else {
            
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
}
