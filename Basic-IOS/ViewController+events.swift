//
//  ViewController+events.swift
//  Basic-IOS
//
//  Created by Savdeep on 04/03/2019.
//  Copyright © 2019 Theoplayer.com. All rights reserved.
//

import Foundation
import THEOplayerSDK

extension ViewController {
    
    func attachEventListeners() {
        self.listeners["play"] = self.theoplayer.addEventListener(type: PlayerEventTypes.PLAY, listener: onPlay)
        self.listeners["pause"] = self.theoplayer.addEventListener(type: PlayerEventTypes.PAUSE, listener: onPause)
    }
    
    func removeEventListeners() {
        self.theoplayer.removeEventListener(type: PlayerEventTypes.PLAY, listener: listeners["play"]!)
        self.theoplayer.removeEventListener(type: PlayerEventTypes.PAUSE, listener: listeners["pause"]!)
    }
    
    func onPlay(event: PlayEvent) {
        print("Play event occured")
    }
    
    func onPause(event: PauseEvent) {
        print("Pause event occured")
    }
    
}
