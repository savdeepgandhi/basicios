//
//  OfflineViewController.swift
//  Basic-IOS
//
//  Created by Savdeep on 04/03/2019.
//  Copyright © 2019 Theoplayer.com. All rights reserved.
//

import UIKit
import THEOplayerSDK
import SystemConfiguration
import GCDWebServers



class OfflineViewController: UIViewController {
    var theoplayer: THEOplayer!
    var listeners : [String: EventListener] = [:]
    
    @IBOutlet weak var PlayerView: UIView!
    func setupTheoPlayer() {
        var frame: CGRect = UIScreen.main.bounds
        frame.size.height = 350
        
        self.theoplayer = THEOplayer()
        self.theoplayer.frame =  frame
        self.theoplayer.addAsSubview(of: PlayerView)
    }
    
    @IBAction func backtoMain(_ sender: Any) {
        self.theoplayer.destroy()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        
        //Setup THEO Player
        setupTheoPlayer()
    
        //Request for Network Interceptor for Downloaded Subtitles
        TheoRequestInterceptor.registerInterceptor()
        self.theoplayer.source = sampleSource
        theoplayer.play();

    }
    
    var sampleSource: SourceDescription {
    
        let textTrack1 = TextTrackDescription(src: "https://127.0.0.1:80/English-subtitle.webvtt", srclang: "English", isDefault: false, kind: .subtitles, label:"English")
        let textTrack2 = TextTrackDescription(src: "https://127.0.0.1:80/Arabic-subtitle.webvtt", srclang: "Arabic", isDefault: false, kind: .subtitles, label:"Arabic")
        let cached = THEOplayer.cache.tasks[0].source
        
        cached.textTracks =  [textTrack1,textTrack2]
        return cached
        
    }
    
    public func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func removeSourceFromCache(source: URL) {
        for cachingTask in THEOplayer.cache.tasks {
            if cachingTask.source.sources[0].src == source{
                print("Will remove caching task \(cachingTask.source.sources[0].src)")
                cachingTask.remove()
            }
        }
    }
    
}
