//
//  ViewController.swift
//  Basic-IOS
//
//  Created by Savdeep on 04/03/2019.
//  Copyright © 2019 Theoplayer.com. All rights reserved.
//

import UIKit
import THEOplayerSDK
import GCDWebServers

class ViewController: UIViewController {
    var theoplayer: THEOplayer!
    var listeners : [String: EventListener] = [:]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let documentsUrl = getDocumentsDirectory()
        let server : GCDWebDAVServer = GCDWebDAVServer(uploadDirectory: documentsUrl.path)
        server.start()
    }
    
    public func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
}

