//
//  theoplayer_ios_sdk_yospace_adapter.h
//  theoplayer-ios-sdk-yospace-adapter
//
//  Created by Myles Jans on 26/11/2018.
//  Copyright © 2018 THEOPlayer. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for theoplayer_ios_sdk_yospace_adapter.
FOUNDATION_EXPORT double theoplayer_ios_sdk_yospace_adapterVersionNumber;

//! Project version string for theoplayer_ios_sdk_yospace_adapter.
FOUNDATION_EXPORT const unsigned char theoplayer_ios_sdk_yospace_adapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <theoplayer_ios_sdk_yospace_adapter/PublicHeader.h>
#import <theoplayer_ios_sdk_yospace_adapter/Yospace.h>
